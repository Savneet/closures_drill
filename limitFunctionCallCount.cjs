function limitFunctionCallCount(cb, n) {
  let count = 0;

  function invokeCB() {
    if (count >= n) {
      console.log("null");
      return null;
    }
    count++;
    return cb();
  }

  return invokeCB;
}
module.exports = limitFunctionCallCount;
