cacheFunctionProblem = require("../cacheFunction.cjs");
function add(a, b) {
  console.log("Adding the values");
  let sum = a + b;
  return sum;
}

let result = cacheFunctionProblem(add);

console.log(result(10, 15));
console.log(result(20, 25));
console.log(result(10, 15));
