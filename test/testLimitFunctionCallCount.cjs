testLimitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function hello() {
  console.log("Say Hello");
}

const invokeCBFunc = testLimitFunctionCallCount(hello, 2); //retuned function stored in invokeCBFunc variable

invokeCBFunc();

invokeCBFunc();

invokeCBFunc();
