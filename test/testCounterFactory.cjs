counterFactoryProblem = require("../counterFactory.cjs");
try {
  console.log(counterFactoryProblem().increment());//counterFactoryProblem will return an object from which we can call it's preperty increment.
  console.log(counterFactoryProblem().decrement());
} catch (error) {
  console.log("There is some error in code");
}
